#describe nodes as
#{module, params} #uninitialized
#{module, params, state} #initialized

#todo
#blackboard
#events

#the root instantiates all the tree
defmodule BT.Root do
  def new(gamestate) do
    {Success, []}
  end
  def tick(state, gamestate) do
    state = case state do
      [head | t] ->
         {Succces, s} = head.tick state, gamestate
         s
      [] ->
         IO.puts "why are we idling?"
         {Succces, s} = empty_nodes gamestate
         s
    end
    state
  end
  def empty_nodes(gamestate) do
    {Success, []} 
  end
end

#a selector continues until one node returns success
defmodule BT.Selector do
  def new(gamestate, subnodes) do
    {Success, subnodes}
  end
  def tick(state, gamestate) do
    state = case state do
      [head | t] ->
         case head.tick(state, gamestate) do
           {Success, state} ->
             {Success, state}
           Fail ->
             {Continue, state}
          end
      [] ->
         Fail
    end
  end
end

#a sec continues until all its nodes got completed
defmodule BT.Sequence do
  def new(gamestate, subnodes) do
    {Success, subnodes} 
  end
  def tick(state, gamestate) do
    state = case state do
      [head | t] ->
         case head.tick(state, gamestate) do
           {Success, state} ->
             {Continue, state}
           Fail ->
             Fail 
          end
      [] ->
         Success
    end
  end
end

defmodule Move do
  def new(_gamestate, params) do
    {Success, params}
  end
  def tick(state, gamestate) do
    {Continue, state}
  end
end

defmodule Failer do 
  def new(_gamestate, _params) do
    Fail
  end
  def tick(_, _) do
    Fail 
  end
end

tree = {BT.Root, {BT.Sequence, [
{BT.Selector, [
  {Move, {100, 100}}
]},
{Bt.Sequence, [
  {Move, {0,0}}
]}
]
}
}

